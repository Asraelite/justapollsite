'use strict';

window.addEventListener('load', init);

function init() {
	var soundElements = Array.from(document.querySelectorAll('.sound'));
	soundElements.forEach(el => {
		var name = el.innerText;
		var audio = new Audio(`audio/${name}.ogg`);
		el.addEventListener('click', _ => audio.play());
	});

	var submitButton = document.querySelector('#submit');
	submitButton.addEventListener('click', _ => {
		submitButton.disabled = true;
		submit();
	});
}

function submit() {
	var forms = Array.from(document.forms);
	var result = {};
	forms.forEach(form => {
		var a = Array.from(form.querySelectorAll('input')).filter(i => i.checked);
		a.forEach(i => {
			result[i.name] = result[i.name] || [];
			result[i.name].push(i.value)
		});
	});
	console.log('data submitted: ' + JSON.stringify(result));

	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = _ => {
        if (xhr.readyState == 4) {
			if (xhr.status == 200) {
				window.location.href = '/results';
			} else {
				document.querySelector('#button').disabled = false;
				alert('Error sending data');
			}
        }
    };
	xhr.open('POST', '/post', true);
	xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
	xhr.send(JSON.stringify(result));
}
