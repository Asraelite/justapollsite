'use strict';

const bodyParser = require('body-parser');
const express = require('express');
const http = require('http');
const jsondb = require('simple-json-db');
const path = require('path');
const nib = require('nib');
const stylus = require('stylus');
const fs = require('fs-extra');

const pollData = require('./data/poll.json');

class Server {
	constructor() {
		this.app = express();
		this.appServer = http.Server(this.app);

		fs.createFileSync(path.resolve('./db.json'));
		this.db = new jsondb(path.resolve('./db.json'));
	}

	start() {
		this.appServer.listen(8080);

		let app = this.app;

		app.set('views', 'public/views');
		app.set('view engine', 'pug');
		app.engine('pug', require('pug').__express);

		app.use(stylus.middleware({
			src: 'public/stylus',
			dest: 'public/static/css',
			force: true,
			compile: (str, path) => {
				return stylus(str)
					.set('filename', path)
					.use(nib())
			}
		}));

		app.use(bodyParser.json());

		app.get('/', (req, res) => {
			let ip = this.getIp(req);
			if (this.ipVoted(ip)) {
				res.redirect('/results');
				return;
			}
			try {
				res.render('index', { poll: pollData });
			} catch(err) {
				console.log('Rendering error:' + err);
				res.render('error/500', { error: err });
			}
		});

		app.get('/results', (req, res) => {
			res.render('results', { results: this.getResults() });
		});

		app.post('/post', (req, res) => {
			this.parseResults(req.body, this.getIp(req));
			res.end('success');
		});

		app.use(express.static('public/static'));

		console.log('Server started');
	}

	getIp(req) {
		return req.connection.remoteAddress + Math.random();
	}

	parseResults(body, ip) {
		let db = this.db;
		if (db.has('ip.' + ip)) return;
		db.set('ip.' + ip, Date.now());
		let count = db.get('responseCount') || 0;
		db.set('responseCount', count + 1);
		db.set('response.' + ip, body);
	}

	getResults() {
		let db = this.db;
		let totals = {};
		let json = db.JSON();
		let responses = [];

		for(var i in json) {
			if (i.split('.')[0] != 'response') continue;

			responses.push(json[i]);
		}

		//console.log(responses);
		responses.forEach(r => {
			for(var i in r) {
				r[i].forEach(v => {
					totals[i] = totals[i] || {};
					totals[i][v] = (totals[i][v] || 0) + 1;
				});
			}
		});

		let result = {
			voicing: {},
			ejectives: {},
			pharyngeals: [],
			website: []
		};

		//console.log(totals);

		pollData.questions.forEach(q => {
			let i = 'v' + q[1];
			totals[i] = totals[i] || [];
			result.voicing[q[1] + '/' + q[2]] = [totals[i][q[1]] | 0, totals[i][q[2]] | 0, totals[i][q[1] + q[2]] | 0];
		});

		pollData.ejectives.forEach(e => {
			let i = 'e' + e;
			totals.ej = totals.ej || {};
			result.ejectives[i] = totals.ej['e' + e] | 0;
		});

		result.pharyngeals = totals['ph'] || {};
		result.website = totals['wb'] || {};

		return {
			count: db.get('responseCount') || 0,
			json: json,
			totals: totals,
			result: result
		};
	}

	ipVoted(ip) {
		return false;
	}
}

module.exports = Server;
